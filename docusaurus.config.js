// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'S.I.G.L.K.',
  tagline: 'a 🇨🇭 Swiss guy in 🇩🇪 Germany learning 🇰🇷 Korean',
  url: 'https://rgunti.gitlab.io/',
  baseUrl: '/siglk/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'rgunti', // Usually your GitHub org/user name.
  projectName: 'siglk', // Usually your repo name.

  presets: [
    [
      '@docusaurus/preset-classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl: 'https://gitlab.com/rGunti/siglk/-/edit/master',
        },
        blog: {
          showReadingTime: true,
          editUrl: 'https://gitlab.com/rGunti/siglk/-/edit/master',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'S.I.G.L.K.',
        logo: {
          alt: 'My Site Logo',
          src: 'img/Flag_of_South_Korea.svg',
        },
        items: [
          {
            to: '/docs/hangul/intro',
            position: 'left',
            label: '한글 - Hangul'
          },
          {
            to: '/docs/other/keyboard',
            position: 'right',
            label: 'Keyboard Reference'
          }
          // {to: '/blog', label: 'Blog', position: 'right'},
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'Tutorial',
                to: '/docs/intro',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'GitLab',
                href: 'https://gitlab.com/rGunti/siglk',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Raphael Guntersweiler. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
