---
sidebar_position: 2
---

# Basic Words

| Word  | Pronounciation | Translation      |
| ----- | -------------- | ---------------- |
| 나비   | `[nabi]`       | butterfly        |
| 도시   | `[doshi]`      | city             |
| 아기   | `[agi]`        | baby             |
| 커피   | `[kopi]`       | coffee           |
| 하마   | `[hama]`       | hippo            |
| 오이   | `[oi]`         | cucumber         |
| 나라   | `[nara]`       | country / nation |
| 파스타 | `[pasta]`       | pasta            |
| 라자냐 | `[lasanya]`     | lasagna          |

## Phrases
잘했어요<br/>
`[tsarleseoyo]`<br/>
_You did a good job! / Well done!_

안녕<br/>
`[anyong]`<br/>
_Bye! / Hi!_
