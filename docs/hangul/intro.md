---
sidebar_position: 1
---

# 한글 Intro

## Conventions in this document
The sounds are described in these boxes: `[a]`.
This transcription should help as a guideline. Where possible
sample words are provided to help but this isn't possible for all
sounds. Some sounds are unique to Korean and cannot be correctly
transcribed using the regular English alphabet. This document
will also not resort to phonetic spelling.

Furthermore, the following characters are used for the transcription
as helpers:
- `/` - describes a sound lying between two other sounds<br/>
  Example: `[r/l]` = a sound between `[r]` and `[l]`
- `|` - describes an abrupt stop
- `_` - highlights the absence of sound
- _UPPERCASE letters_ highlight stress on certain sounds<br/>
  Example: `[eL]` highlights stress on `[l]` making `[e]` more quiet

## Consonants
### Basic Consonants
- **ㄱ** - `[g]` as in "gun"
- **ㅋ** - `[k]` as in "kill"
- **ㄴ** - `[n]` as in "nose"
- **ㄷ** - `[d]` as in "door"
- **ㅌ** - `[t]` as in "two doors"
- **ㄹ** - `[r/l]` as in "rattlesnake"
- **ㅁ** - `[m]` as in "mouth"
- **ㅂ** - `[b]` as in "bucket"
- **ㅅ** - `[s]` as in "standing"
- **ㅈ** - `[j]`
- **ㅊ** - `[ch]`
- **ㅍ** - `[p]` as in "Part II"
- **ㅎ** - `[h]` as in "hat"
- **ㅇ** - `[_]` - does not make any sound / connector

### Double Consonants
- **ㄲ** - `[c/q]` as in "què" (spanish)
- **ㅆ** - `[s]` as in "sun"
- **ㄸ** - `[t]` as in "très" (spanish); soft t
- **ㅃ** - `[p]` as in "apple"
- **ㅉ** - `[j/z]` - "zhèn"

## Vowels
_The vowels are written to the right of consonants._

- **ㅏ** - `[a]`
- **ㅑ** - `[ya]`
- **ㅓ** - `[u]`
- **ㅕ** - `[yu]`
- **ㅣ** - `[i]`
- **ㅐ** - `[e]` as in "apple"
- **ㅔ** - `[e]` as in "apple" (again)
- **ㅒ** - `[ye]`
- **ㅖ** - `[ye]`

:::info
Double lines in vowels add a `[y]` sound as a prefix.
:::

### "Down Vowels"
_These vowels are written below the consonants._

- **ㅗ** - `[o]`
- **ㅛ** - `[yo]`
- **ㅜ** - `[u]`
- **ㅠ** - `[yu]`
- **ㅡ** - `[eu]`

### W Vowels
- **ㅚ** - `[wae]`
- **ㅙ** - `[wae]` (again)
- **ㅘ** - `[wa]`
- **ㅟ** - `[wi]`
- **ㅝ** - `[wo]`
- **ㅞ** - `[wae]` (again)
- **ㅢ** - `[ui]`

## 받침 - `[batchim]`
받침 is the third character in a set. It is always placed at the bottom.

:::caution
ㄸ, ㅃ and ㅉ cannot become 받침!
:::

The following section will only cover a few examples to get started:

- **ㄱ** in **낙** - `[na_]` - `[g]` is dropped
- **ㄴ** in **긴** - `[kin]` - adds an `[n]`
- **ㄷ** in **닫** - `[ta_]` - `[d]` is dropped
- **ㄹ** in **달** - `[tal]` - adds an `[eL]`
- **ㅁ** in **남** - `[namm]` - adds an `[m_]`
- **ㅂ** in **밥** - `[pa|]` - adds an abrubt stop
- **ㅅ** in **깃** - `[ki|]` - adds an abrubt stop
- **ㅇ** - adds an `[ng]` sound
- **ㅈ** in **겆** - `[ko|]`
- **ㅊ** in **읓** - `[jo|]`
- **ㅋ** in **부엌** - `[buo|]`
- **ㅍ** in **섶** - `[so|]`
- **ㅎ** in **좋아** - `[joa]` - either binds to the next vowel or doesn't produce any sound

## On the Topic of Stroke Order
Hangul characters have a pre-defined stroke order. The stroke order follows this format:
- top to bottom
- left to right
