# Keyboard Layout

For your reference: This is the Korean Keyboard layout, if you want to use it for typing.

![South Korean Keyboard Layout](/img/KB_South_Korea.svg)

_Source: wikipedia.org (by Yes0song under CC BY-SA 3.0, https://commons.wikimedia.org/wiki/File:KB_South_Korea.svg)_

## Other Links
- [Korean Online Keyboard](https://www.branah.com/korean) - Can be used to type Hangul in a text box to copy and paste it somewhere else later
