---
sidebar_position: 1
---

# Intro

Let's get started with Korean. First steps: Learn the Korean writing system, [Hangul](hangul/intro), then continue onwards.
